package main

import (
	"math/rand"
	"strconv"
	"time"

	"github.com/docker/go-plugins-helpers/secrets"
	"github.com/dustinkirkland/golang-petname"
)

type SecretPetsDriver struct {
}

func (p SecretPetsDriver) Get(req secrets.Request) secrets.Response {
	onePetPerTask, _ := strconv.ParseBool(req.SecretLabels["one_pet_per_task"]) // Defaults to false if error returned
	return secrets.Response{
		Value:      []byte(petname.Generate(3, " ")),
		DoNotReuse: onePetPerTask,
	}
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	d := SecretPetsDriver{}
	h := secrets.NewHandler(d)
	h.ServeUnix("plugin", 0)
}
