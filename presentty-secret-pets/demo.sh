#!/bin/bash
set -u
set -o pipefail
export plugin_name=${plugin_name:-sirlatrom/secret-pets:latest}
export remote=${remote:-${plugin_name}}
./cleanup.sh &>/dev/null

installer
until [[ "$(docker plugin inspect ${plugin_name} --format '{{.Enabled}}' 2>/dev/null)" == "true" ]]
do
    echo "waiting for plugin to be installed"
    sleep 1
done

docker node ls --filter role=worker -q | wc -l | grep -q 0 && snitch_role=manager || snitch_role=worker
export snitch_role
docker stack deploy --compose-file docker-compose.yml demo
docker service logs -f demo_snitch
exit $?
