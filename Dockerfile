FROM golang:1.12-alpine AS build
ARG repo=gitlab.com/sirlatrom/secret-pets
WORKDIR /go/src
COPY vendor/ ./
RUN CGO_ENABLED=0 go install -v ./...
WORKDIR /go/src/$repo
COPY main.go .
RUN CGO_ENABLED=0 go install -v

FROM scratch
COPY --from=build "/go/bin/secret-pets" "/secret-pets"
ENTRYPOINT ["/secret-pets"]
